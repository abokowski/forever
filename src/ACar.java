
public abstract class ACar implements ICar {
	
	/**
	 * make of the car
	 */
	private String make;
	
	/**
	 * model of the car
	 */
	private String model;
	
	/**
	 * year of the car
	 */
	private int year;
	
	/**
	 * mileage on the car
	 */
	private int mileage;
	
	/**
	 * Constructor for abstract car
	 * 
	 * @param inMake make of the car
	 * @param inModel model of the car
	 * @param inYear year of the car
	 */
	public ACar(String inMake, String inModel, int inYear) {
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMileage(int inMileage)
	{
		mileage = inMileage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel() {
		return model;
	}
	
	@Override
	public int getMileage() {
		return mileage;
	}
	
	@Override
	public int getYear() {
		return year;
	}
	
	@Override
	public String toString() {
		return getMake() + " " + getModel();
	}

	
	
	
}
